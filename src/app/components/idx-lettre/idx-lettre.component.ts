import {Component, Input, OnInit} from '@angular/core';
import {RecettesInterface} from "../../models/recettes-interface";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-idx-lettre',
  templateUrl: './idx-lettre.component.html',
  styleUrls: ['./idx-lettre.component.css']
})
export class IdxLettreComponent implements OnInit{

  recette_in : RecettesInterface[] = [];
  @Input()firstLetter:string = " - "
  alphabet:string[] = [    "A",    "B",    "C",    "D",    "E",    "F",    "G",    "H",    "I",    "J",    "K",    "L",    "M",    "N",    "O",    "P",    "Q",    "R",    "S",    "T",    "U",    "V",    "W",    "X",    "Y",   "Z",  ];

  constructor(public recetteService:DataService) {
  }
  ngOnInit() {
    this.recetteService.findAll().subscribe(value => this.recette_in = value)
  }

filterByLetter(lettre: string){
      return  this.recette_in!.filter(x => x.titre.at(0)?.toUpperCase() === lettre.toUpperCase())

}
}
