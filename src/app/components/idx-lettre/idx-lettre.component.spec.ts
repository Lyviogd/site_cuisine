import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdxLettreComponent } from './idx-lettre.component';

describe('IdxLettreComponent', () => {
  let component: IdxLettreComponent;
  let fixture: ComponentFixture<IdxLettreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdxLettreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IdxLettreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
