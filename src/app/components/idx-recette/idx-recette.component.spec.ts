import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdxRecetteComponent } from './idx-recette.component';

describe('IdxRecetteComponent', () => {
  let component: IdxRecetteComponent;
  let fixture: ComponentFixture<IdxRecetteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdxRecetteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IdxRecetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
