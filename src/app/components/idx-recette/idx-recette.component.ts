import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {RecettesInterface} from "../../models/recettes-interface";
import {Router} from "@angular/router";

@Component({
  selector: 'app-idx-recette',
  templateUrl: './idx-recette.component.html',
  styleUrls: ['./idx-recette.component.css']
})
export class IdxRecetteComponent {
  @Input() recetteInput!:RecettesInterface;
  @Output() selectedRecette : EventEmitter<RecettesInterface> = new EventEmitter();

  constructor(private router:Router) {
  }



  @HostListener('click') call_parent(){
   this.router.navigate(['/recette',this.recetteInput.id])
 }

}
