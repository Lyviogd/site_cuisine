import {Component, OnInit} from '@angular/core';
import {RecettesInterface} from "../../models/recettes-interface";
import {DataService} from "../../services/data.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.css']
})
export class RecetteComponent implements OnInit{

  recette_detail:RecettesInterface | undefined

  constructor(public dataService:DataService, private route:ActivatedRoute) {
  }
  ngOnInit() {
    let array_value;
    let id = this.route.snapshot.paramMap.get('id')



    this.dataService.obs_findOne(Number(id)).subscribe(value => this.recette_detail = value[0])

  }


}
