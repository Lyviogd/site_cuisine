import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../services/data.service";
import {RecettesInterface} from "../../models/recettes-interface";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements  OnInit{

  recettes_json : RecettesInterface[] = []

  constructor(private service_d:DataService) {
  }
  ngOnInit() {
    this.service_d.findAll().subscribe(value => this.recettes_json = value);
    console.log(this.recettes_json)
  }



}
