export interface RecettesInterface {
  "id": number,
  "titre": string,
  "image"?: string,
  "ingredients"?: ingredient[],
  "etape"?: string[],
  "imageetape"?: string[],
  "recetteservice"?: number,
  "notes"?: string
}

export interface ingredient{
  "ressource"?:string,
  "quantity"?: number,
  "unity"?: string
}
