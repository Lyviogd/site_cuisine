import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {filter, from, map, mergeMap, Observable} from "rxjs";
import {RecettesInterface} from "../models/recettes-interface";



@Injectable({
  providedIn: 'root'
})
export class DataService {

  json_recipes!:RecettesInterface;

  constructor(private http:HttpClient) {  }

  findAll():Observable<RecettesInterface[]>{
    return this.http.get<RecettesInterface[]>('assets/json/recettes.json')
  }

  findOne(id: number):RecettesInterface {
    let tmp_rec!: RecettesInterface
    this.findAll().forEach(value => value.filter(value1 => {
      if(value1.id === id) {
        this.json_recipes = value1;
        console.log(value1)
      }
    }))

    return  this.json_recipes;
  }

  
  obs_findOne(id:number):Observable<any>{
    
    return this.http.get<any | RecettesInterface[]>('assets/json/recettes.json')
    .pipe(
      
/*      map(rec => rec.filter((value: RecettesInterface | any)  => {
        if(value.id == id){
          return value  
        }
      })
            )
      ) https://rxjs.dev/api/operators/mergeMap
      */

      map(rec => rec.filter((value: RecettesInterface | any)  => {
        if(value.id == id){
          return value
        }
      }))
      
      ) 
  }
  
}
