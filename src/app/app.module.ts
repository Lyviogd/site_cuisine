import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BanniereComponent } from './components/banniere/banniere.component';
import { HomeComponent } from './components/home/home.component';
import { RecetteComponent } from './components/recette/recette.component';
import { IndexComponent } from './components/index/index.component';
import { MediaComponent } from './components/media/media.component';
import {RouterModule, Routes} from "@angular/router";
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import {HttpClientModule} from "@angular/common/http";
import { AboutComponent } from './components/about/about.component';
import { IdxRecetteComponent } from './components/idx-recette/idx-recette.component';
import { IdxLettreComponent } from './components/idx-lettre/idx-lettre.component';

const  routes:Routes=[
  {path:'home',component:HomeComponent},
  {path:'index',component:IndexComponent},
  {path:'recette/:id',component:RecetteComponent},
  {path:'about',component:AboutComponent},

  {path : "", redirectTo:"home",pathMatch:"full"},
  {path: "**", component:PageNotFoundComponent}

]
@NgModule({
  declarations: [
    AppComponent,
    BanniereComponent,
    HomeComponent,
    RecetteComponent,
    IndexComponent,
    MediaComponent,
    PageNotFoundComponent,
    AboutComponent,
    IdxRecetteComponent,
    IdxLettreComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
